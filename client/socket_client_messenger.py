from message_object import MessageObject
import socket


class SocketClientMessenger(object):
    def __init__(self):
        pass


    def connect(self, host, port):
        self.ss = socket.socket() 
        self.ss.connect((host, port))
        return self


    def send(self, message):
        self.ss.send(repr(message) + '\n')
        return self


    def recv(self):
        return MessageObject.from_string(self.ss.recv(4096))


    def disconnect(self):
        self.ss.close()
        return self

