from message_object import MessageObject
from decorator import decorator


class MSWClientException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


class MSWClientError(MSWClientException):
    pass


@decorator
def message_dialog(method, self, *args, **kwargs):
    try:
        out_msg = method(self, *args, **kwargs)
        self.messenger.send(out_msg)
        in_msg = self.messenger.recv()

        if in_msg.status == 'error':
            raise MSWClientError('Server returned error status with message: {}'.format(in_msg.msg))

        return in_msg

    except MSWClientError as ex:  
        raise ex

    except Exception as ex:
        raise MSWClientException('Something wrong with messenger: {}'.fomrat(ex.msg))



def required_fields_in_answer(*fields):
    @decorator
    def wrapper(method, self, *args, **kwargs):
        in_msg = method(self, *args, **kwargs)
        for field in fields:
            if in_msg[field] is None:
                raise MSWClientException('Invalid server answer {0}. Field {1} is required'.format(repr(in_msg), field))
        return in_msg
    return wrapper


class MSWClient(object):
    def __init__(self, messenger):
        self.messenger = messenger

    @required_fields_in_answer('status')
    @message_dialog
    def start(self, height, width, mine_count):
        return MessageObject(
            action='start',
            height=height,
            width=width,
            mine_count=mine_count
        )

    @required_fields_in_answer('status', 'updates')
    @message_dialog
    def hit(self, i, j):
        return MessageObject(
            action='hit',
            row=i,
            column=j
        )

