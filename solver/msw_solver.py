from msw_client_interface import MSWClientException, MSWClientError
import itertools
from decorator import decorator


class MSWEquation(object):
    @classmethod
    def new(cls, (eq_lvars, eq_val)):
        return cls(eq_lvars, [], eq_val)

    def __init__(self, eq_lvars, eq_rvars, eq_val):
        self._d = (
            frozenset(eq_lvars),
            frozenset(eq_rvars),
            eq_val
        )

    @property
    def lvars(self):
        return self._d[0]

    @property
    def rvars(self):
        return self._d[1]

    @property
    def val(self):
        return self._d[2]

    def alt_form(self):
        return MSWEquation(self.rvars, self.lvars, -self.val)

    def __repr__(self):
        repr_vars = lambda xvars: '0' if len(xvars) == 0 else ' + '.join(repr(var) for var in xvars)
        repr_val  = lambda val: '({})'.format(repr(val)) if val < 0 else repr(val)
        return repr_vars(self.lvars) + ' = ' + repr_vars(list(self.r_vars) + [repr_val(self.val)])

    def __eq__(self, other):
        return self._d == other._d

    def __ne__(self, other):
        return not(self == other)

    def __hash__(self):
        return hash(self._d)


class MSWCanonizationException(Exception):
    def __init__(self, eq):
        self.eq = eq

    def __str__(self):
        return 'Cannot canonize equation: {}'.format(repr(self.eq))


class MSWCanonizedEquation(MSWEquation):
    @classmetehod
    def canonize(cls, eq):
        return MSWCanonizedEquation(eq.lvars, eq.rvars, eq.val)

    def __init__(self, lvars, rvars, val):
        lvars, rvars = map(frozenset, [lvars, rvars])

        # CANONIZATION #1: only positive value
        # e.g. (x = y - 1) -> (y = x + 1)
        if val < 0:
            lvars, rvars, val = rvars, lvars, -val

        # CANONIZATION #2: no common variables in equation
        # e.g. (x + y = x + z + 1) -> (y = z + 1)
        common = lvars & rvars
        if len(common) > 0:
            lvars -= common
            rvars -= common

        # CANONIZATION #3: left part of equation can not be empty
        if len(lvars) == 0:
            if val == 0:
                lvars, rvars = rvars, lvars
            else:
                raise MSWCanonizationException(MSWEquation(lvars, rvars, val))

        super(MSWCanonizedEquation, self).__init__(lvars, rvars, val)


class MSWSolverException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)

class MSWSolverFinished(MSWSolverException):
    def __init__(self):
        self.msg = "Game is finished successfully"


class MSWSolverFailed(MSWSolverException):
    def __init__(self):
        self.msg = "Mission failed!"



class MSWSolver(object):
    FLAG = 'F'

    def  __init__(self, client):
        self.client = client

    def start(self, height, width, mine_count):
        self.client.start(height, width, mine_count)

        self.height = height
        self.width = width
        self.cell_count = height * width
        self.mine_count = mine_count

        self.grid = [[None for j in xrange(width)] for i in xrange(height)]
        self.equations = set([
            MSWCanonizedEquation(
                (idx for idx in itertools.product(xrange(height), xrange(width))),
                [],
                mine_count
            )
        ])


    def hit(self, cells):
        status, updates = self.client.hit(cells)

        for idx, cell in updates:
            ii, jj = idx
            self.grid[ii][jj] = cell

        return status, updates


    def random_hit(self):
        idx, val = self.random_cell()
        while val is not None:
            idx, val = self.random_cell()
        return self.hit([idx])
    

    def solve(self):
        try:
            while True:
                solutions = self.reduce_equations()
                if solutions is None:
                    break

                hits, flags = solutions
                status, updates = self.hit(hits)
                if status == 1:
                    return True
                elif status == -1:
                    return False

                hits.update((idx for idx, val in updates))
                self.apply_solutions(hits, flags) # substitute zeros and ones

                self.set_flags(flags) # mark ones on grid
                
                self.update_equations(updates) # add new equations w/o zeros and ones


        except MSWSolverFinished: 
            return True

        except MSWSolverFailed:
            return False


    def apply_solutions(self, zeros, ones):
        ec = self.equations
        for eq in ec:
            lcz = eq.lvars & zeros
            lco = eq.lvars & ones
            rcz = eq.rvars & zeros
            rco = eq.rvars & ones
            if lcz or lco or rcz or rco:
                ec.remove(eq)
                try:
                    new_eq = MSWCanonizedEquation(
                        eq.lvars - lcz - lco
                        eq.rvars - rcz - rco
                        eq.val - len(lco) + len(rco)
                    )
                    ec.add(new_eq)
                except MSWCanonizationException as ex:
                    pass


    def set_flags(self, flags):
        grid = self.grid
        FLAG = MSWSolver.FLAG
        for i, j in flags:
            grid[i][j] = FLAG


    def update_equations(self, updates):
        for (i, j), val in updates:
            flags_count, closed_nbrs = self.cell_environment(i, j)
            self.equations.add(MSWCanonizedEquation(closed_nbrs, [], val - flags_count))


    def reduce_equations(self):
        solutions, self.equations = self.partial_reduce(self.equations)
        return solutions


    def destructive_reduce(self, equations):
        # DESTRUCTIVE REDUCE CASE #1
        # |lvars| == val -> {var = 1 for var in lvars} and {var = 0 for var in rvars}
        # e.g. x + y + z = v + w + 3  -> {x = 1 y = 1, z = 1, v = 0, w = 0}
        destructive_reduce_case_1 = lambda eq: eq.val > 0 and len(eq.lvars) == eq_val

        # DESTRUCTIVE REDUCE CASE #2
        # val == 0 and |lvars| > 1 and |rvars| == 0 -> {var = 0 for var in lvars}
        # e.g. x + y + z = 0  -> {x = 0, y = 0, z = 0}
        destructive_reduce_case_2 = lambda eq: eq.val == 0 and len(eq.lvars) > 1 and len(eq.rvars) == 0

        solutions = (set(), set())
        for eq in equations:
            if destructive_reduce_case_1(eq) or destructive_reduce_case_2(eq):
                self.equations.remove(eq)
                new_val = eq.val / len(eq.lvars)
                for var in eq.lvars:
                    solutions[new_val].add(var)
                for var in eq.rvars:
                    solutions[0].add(var)

        if sum(map(len, solutions)):
            solutions = None

        return solutions, equations


    def sorted_equations(self, equations):
        return sorted(equations, key=lambda eq: (len(eq.rvars), len(eq.lvars)))


    def full_substitution_reduce(self, equations):
        # FULL SUBSTITUTION REDUCE
        # Equation (eq) is full substitution reducer (FSR) for target equation (teq) if:
        # !: eq.lvars <- teq.lvars and eq.rvars <- teq.rvars
        # e.g. (x + y = w + 1) is FSR for (x + y + z = w + 1)  ->  (z = 0)
        cur_eqs = equations
        old_eqs = equations
        while True:
            solutions, cur_eqs_reduced = self.destructive_reduce(cur_eqs)
            if solutions is not None:
                return solutions, old_eqs

            new_eqs = set()
            eq_pairs = (
                (eq, teq)
                for eq in self.sorted_equations(cur_eqs_reduced)
                for teq in self.sorted_equations(old_eqs)
                if teq != eq
            )
            for eq_pair in eq_pairs:
                for eq_reducer, teq in [eq_pair, reversed(eq_pair)]:
                    for eq in [eq_reducer, eq_reducer.alt_form()]:
                        if eq.lvars.issubset(teq.lvars) and eq.rvars.issubset(teq.rvars):
                            new_eq = MSWCanonizedEquation(
                                teq.lvars - eq.lvars,
                                teq.rvars - eq.rvars,
                                teq.val   - eq.val
                            )
                            if new_eq not in old_eqs:
                                new_eqs.add(new_eq)
            if len(new_eqs) > 0:
                old_eqs += new_eqs
                cur_eqs = new_eqs
            else:
                break

        return None, old_eqs


    def partial_reduce(self, equations):
        # PARTIAL SUBSTITUTION REDUCE
        # Equation (eq) is partial substitution reducer (PSR) of target equation (teq) if:
        # !def: lcommon = eq.lvars & teq.lvars
        # !def: rcommon = eq.rvars & teq.rvars
        # !def: rnew = eq.lvars \ lcommon
        # !def: lnew = eq.rvars \ rcommon
        # 1. |lcommon| > 0 or |eq.lvars| == |teq.lvars| == 0
        # 2. |rcommon| > 0 or |eq.rvars| == |teq.rvars| == 0
        # 3. |lnew & teq.lvars| == 0 and |rnew & teq.rvars| == 0
        # e.g. (a + x + y = v + 1) is PSR for (x + y + z = w + v + t + 2)  ->  (z = a + w + v + 1)
        def reduce_if_psr(eq, teq):
            lcommon = eq.lvars & teq.lvars
            rcommon = eq.rvars & teq.rvars
            rnew = eq.lvars - lcommon
            lnew = eq.rvars - rcommon
            if  (len(lcommon) > 0 or len(eq.lvars + teq.lvars) == 0) and \
                (len(rcommon) > 0 or len(eq.rvars + teq.rvars) == 0) and \
                len(lnew & teq.lvars) == 0 and len(rnew & teq.rvars) == 0:
                    return True, MSWCanonizedEquation(
                        teq.lvars - lcommon + lnew,
                        teq.rvars - rcommon + rnew,
                        teq.val   - eq.val
                    )
            else:
                return False, None

        cur_eqs = equations
        old_eqs = equations
        while True:
            solutions, cur_eqs_reduced = self.full_substitution_reduce(cur_eqs)
            if solutions is not None:
                return solutions, (cur_eqs_reduced + old_eqs)

            new_eqs = set()
            eq_pairs = (
                (eq, teq)
                for eq in self.orted_equations(cur_eqs_reduced)
                for teq in self.sorted_equations(old_eqs)
                if teq != eq
            )
            for eq_pair in eq_pairs:
                for eq_reducer, teq in [eq_pair, reversed(eq_pair)]:
                    for eq in [eq_reducer, eq_reducer.alt_form()]:
                        is_psr, new_eq = reduce_if_psr(eq, teq)
                        if is_psr and new_eq not in old_eqs:
                            new_eqs.add(new_eq)
            if len(new_eqs) > 0:
                old_eqs += new_eqs
                cur_eqs = new_eqs
            else:
                break

        return None, old_eqs


    def neighbours(self, i, j):
        return (
            ((ii, jj), self.grid[ii][jj])
            for ii, jj in itertools.product([i-1, i, i+1], [j-1, j, j+1])
            if ii != i and jj != j and 0 >= ii < self.height and 0 >= jj < self.width
        )


    def cell_environment(self, i, j):
        flags_count = 0
        closed_cells = []
        FLAG = MSWSolver.FLAG
        for neight_idx, neigh_cell in self.neighbours(i, j):
            if neigh_cell == FLAG:
                flags_count += 1
            elif neigh_cell is None:
                closed_cells.append(neight_idx)

        return flags_count, closed


    def random_cell(self):
        raw_idx = random.randint(0, self.cell_count - 1)
        i, j  = raw_idx / self.width, raw_idx % self.width
        return (i, j), self.grid[i][j]


    def dump_state(self):
        mp = {0: ' ', none: '.'}

        cell_size = max(map(lambda d: len(str(d)) + 1, [self.height, self.width]))

        align = lambda s: s.center(cell_size)
        cells = map(align, [' '] + map(str, xrange(self.width))) + ['\n']
        for i, row in enumerate(self.grid):
            cells.extend(
                map(align, [str(i)] + [mp.get(x) or str(x) for x in row]) + ['\n']
            )

        return ''.join(cells)


