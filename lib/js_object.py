import json


class JSObjectSerializationError(Exception):
    def __str__(self):
        return 'Failed to serialize object'


class JSObjectDeserializationError(Exception):
    def __init__(self, source):
        self.source = source

    def __str__(self):
        return 'Failed to deserialize JSObject from: {}'.format(repr(self.source))


class JSObject(object):
    def __init__(self, **kwargs):
        self.__dict__ = kwargs

    def __getattr__(self, name):
        return self.__dict__.get(name)

    def __getitem__(self, name):
        return self.__dict__.get(name)

    def __setitem__(self, name, value):
        self.__dict__[name] = value

    def __repr__(self):
        try:
            return json.dumps(self, default=lambda jso: jso.__dict__)
        except:
            raise JSObjectSerializationError()

    @classmethod
    def from_dict(cls, dd):
        return JSObject(**dd)

    @classmethod
    def from_string(cls, json_string):
        try:
            return JSObject.from_dict(json.loads(json_string))
        except:
            raise JSObjectDeserializationError(json_string)

