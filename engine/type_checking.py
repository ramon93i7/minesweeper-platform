from decorator import decorator


class TypeChecker(object):
    def __init__(self, exception_factory):
        self.exception_factory = exception_factory

    def __call__(self, *args_types, **kwargs_types):
        @decorator
        def wrapper(fn, *args, **kwargs):
            for idx, (arg, tp) in enumerate(zip(args, args_types)):
                if tp and not isinstance(arg, tp):
                    raise self.exception_factory('Type of arg #{0} must be {1}'.format(idx, tp))

            for kwarg_key, kwarg_val in kwargs.iteritems():
                tp = kwargs_types.get(kwarg_key)
                if tp and not isinstance(kwarg_val, tp):
                    raise self.exception_factory('Type of kwarg "{0}" must be {1}'.format(kwarg_key, tp))

            return fn(*args, **kwargs)
        return wrapper

