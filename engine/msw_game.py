import random
import itertools
from decorator import decorator
from type_checking import TypeChecker
import json


class MSWGameException(Exception):
    def __init__(self, msg, game=None):
        self.msg = msg
        self.game = game

    def __str__(self):
        return repr(self.msg)


class MSWGameTypeException(MSWGameException):
    pass

accept_types = TypeChecker(MSWGameException)


class MSWGameCreationException(MSWGameException):
    pass


class MSWGameHitException(MSWGameException):
    pass


class MSWGame(object):
    FAILED, OK, FINISHED = -1, 0, 1
    BOMB = '@'

    __slots__= ('height', 'width', 'mine_count', 'status', 'grid', 'rest', 'log')

    @accept_types(None, int, int, int, bombs=list, log=list)
    def __init__(self, height, width, mine_count, bombs=None, log=None):
        if height < 1 or width < 1:
            raise MSWGameCreationException('Invalid size of game: ({0})x({1})'.format(height, width))

        size = height * width
        if size < mine_count:
            raise MSWGameCreationException('Mine count bigger than grid size')


        self.status = MSWGame.OK
        self.width = width
        self.height = height
        self.mine_count = mine_count
        self.grid = [[None] * width for _ in xrange(height)]
        self.rest = size - mine_count

        bombs = bombs or (
            (idx / width, idx % width)
            for idx in random.sample(xrange(size), mine_count)
        )
        for i, j in bombs:
            self.grid[i][j] = MSWGame.BOMB

        self.log = []
        if log:
            for i, j in log:
                self.hit(i, j)

    @classmethod
    def from_string(cls, dump):
        d = json.loads(dump)
        fields = ['height', 'width', 'mine_count', 'bombs', 'log']
        return cls(*map(d.get, fields))

    def to_string(self):
        r = {}
        r['height'] = self.height
        r['width'] = self.width
        r['mine_count'] = self.mine_count
        r['log'] = self.log
        r['bombs'] = [
            (i, j)
            for i, j in itertools.product(xrange(self.height), xrange(self.width))
            if self.grid[i][j] == MSWGame.BOMB
        ]
        return json.dumps(r)

    @decorator
    def _log(method, self, *args, **kwargs):
        result = method(self, *args, **kwargs)
        self.log.append(args)
        return result

    @accept_types(None, int, int)
    @_log
    def _hit(self, i, j):
        if 0 > i <= self.height or 0 > j <= self.width:
            raise MSWGameHitException('Cell index is out of range', self)

        if self.grid[i][j] == MSWGame.BOMB:
            self.status = MSWGame.FAILED
            return [((i, j), -1)]

        # Smart updates
        updates = []
        queue = [(i, j)]
        for ii, jj in queue:
            if self.grid[ii][jj] is None:
                value = sum(1 for _ in self._bombs_around(ii, jj))
                self.grid[ii][jj] = value
                self.rest -= 1
                updates.append(((ii, jj), value))
                if value == 0:
                    queue.extend(self._neighbours(ii, jj))

        if not self.rest:
            self.status = MSWGame.FINISHED

        return updates


    def hit(self, idxs):
        if self.status != MSWGame.OK:
            raise MSWGameHitException('Game is over, dude!', self)
        all_updates = []
        for i, j in idxs:
            updates = self._hit(i, j)
            all_updates.extend(updates)
        return all_updates


    @accept_types(None, bool)
    def dumps(self, bombs=False):
        mp = {0: ' ', None: '.'}
        if not bombs:
            mp[MSWGame.BOMB] = '.'

        cell_size = max(map(lambda d: len(str(d)) + 1, [self.height, self.width]))

        align = lambda s: s.center(cell_size)
        cells = map(align, [' '] + map(str, xrange(self.width))) + ['\n']
        for i, row in enumerate(self.grid):
            cells.extend(
                map(align, [str(i)] + [mp.get(x) or str(x) for x in row]) + ['\n']
            )

        return ''.join(cells)


    @accept_types(None, bool)
    def show(self, bombs=False):
        print self.dumps(bombs)

    def __repr__(self):
        return self.dumps(True)

    def _neighbours(self, i, j):
        return (
            (ii, jj)
            for ii, jj in itertools.product([i-1, i, i+1], [j-1, j, j+1])
            if 0 <= ii < self.height and 0 <= jj < self.width
        )
        
    def _bombs_around(self, i, j):
        return (
            (ii, jj)
            for ii, jj in self._neighbours(i, j)
            if self.grid[ii][jj] == MSWGame.BOMB
        )
    
