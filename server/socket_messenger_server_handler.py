from message_object import MessageObject


class SocketMessengerServerHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        while True:
            request_string = self.rfile.readline().strip()
            if request_string:
                in_message = MessageObject.from_string(request_string)
                out_message = self.handle_message(in_message)
                self.wfile.write(repr(out_message) + '\n')

    def handle_message(self, in_msg):
        raise NotImplementedError

