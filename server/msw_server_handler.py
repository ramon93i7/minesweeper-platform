from msw_game import MSWGame
from decorator import decorator
from message_object import MessageObject


class MSWServerException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


def required_fields(*fields):
    @decorator
    def wrapper(method, self, msg):
        for field in fields:
            if msg[field] is None:
                raise MSWServerException('Missing required field: <{0}>'.format(field))

        return method(self, msg)

    return wrapper


class MSWServerHandler(object):
    def handle_message(self, in_msg):
        try:
            return self.dispatch_by_action(in_msg)

        except MSWServerException as ex:
            return MessageObject(status='error', msg=ex.msg)


    @required_fields('action')
    def dispatch_by_action(self, in_msg):
        action = 'do_{}_action'.format(in_msg.action)

        if action not in dir(self):
            raise MSWServerException('Unknown action: {}'.format(action))

        action_handler = getattr(self, action)
        out_msg = action_handler(in_msg)
        #out_msg.src = in_msg # tracing patch

        return out_msg


    @required_fields('type')
    def do_start_action(self, msg):
        pass # TODO: implement
        self.game = MSWGame().start(msg.height, msg.width, msg.mine_count)
        return MessageObject(status='ok')


    @required_fields('gid', 'row', 'column')
    def do_hit_action(self, msg):
        if self.game is None or not self.game.started:
            raise MSWServerException('Game is not started. Use <start> action.')

        status, updates = self.game.hit(self.row, self.column)
        if status == MSWGame.OK:
            return MessageObject(status='ok', updates=updates)
        elif status == MSWGame.FINISHED:
            return MessageObject(status='finished', updates=updates)
        elif status == MSWGame.FAILED:
            return MessageObject(status='failed', updates=updates)

