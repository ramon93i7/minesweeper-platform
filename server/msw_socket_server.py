from msw_server_handler import MSWServerHandler
from socket_messenger_server_handler import SocketMessengerServerHandler
import SocketServer


class MSWSocketServerHandler(MSWServerHandler, SocketMessengerServerHandler):
    pass


class MSWSocketServer(SocketServer.ForkingTCPServer):
    def __init__(self, host, port):
        super(MSWServer, self).__init__((host, port), MSWSocketServerHandler)


